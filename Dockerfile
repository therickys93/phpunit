FROM php:7
RUN apt-get update -yqq
RUN apt-get install git -yqq
RUN curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-5.7.10.phar && chmod +x /usr/local/bin/phpunit
RUN docker-php-ext-install mysqli
RUN pecl install xdebug-2.9.8
RUN docker-php-ext-enable xdebug
