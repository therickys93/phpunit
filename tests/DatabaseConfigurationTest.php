<?php

    class DatabaseConfigurationTest extends PHPUnit_Framework_TestCase {

        public function testHost() {
            $this->assertEquals("localhost", DatabaseConfiguration::host());
        }

        public function testHostFromVariable() {
            putenv("DATABASE_HOST=host");
            $this->assertEquals("host", DatabaseConfiguration::host());
            putenv("DATABASE_HOST");
        }

    }

?>
